(setq gnutls-algorithm-priority "NORMAL:-VERS-TLS1.3")

(setq max-lisp-eval-depth 10000)
(setq max-specpdl-size 10000)
;; 800kB par défaut, donne plus de mémoire lors du lancement du fichier
(setq gc-cons-threshold (* 25 1000 1000))

(require 'package)

(setq package-enable-at-startup nil)
(add-to-list 'package-archives
	     '("melpa" . "https://melpa.org/packages/"))
(add-to-list 'package-archives
 	     '("gnu" . "https://elpa.gnu.org/packages/"))
(add-to-list 'package-archives
	     '("melpa3" . "http://www.mirrorservice.org/sites/stable.melpa.org/packages/"))
(package-initialize)

;; Bootstrap `use-package'
(unless (package-installed-p 'use-package)
  (package-refresh-contents)
  (package-install 'use-package))

(setq custom-file (expand-file-name "custom.el" user-emacs-directory))
(load-file custom-file)
;; Fichier de configuration
(org-babel-load-file (expand-file-name "myinit.org" user-emacs-directory))


;; On lui permet un seul Giga de mémoire avant chaque passage du ramasse miette
(setq gc-cons-threshold (* 1 1000 1000))
