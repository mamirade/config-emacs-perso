;; Fichier de configuration donné pour la programmation en TP de scheme

;; Fichier (en théorie) présent par défaut sur les installations "modernes" d'Emacs
(load "quack.el")
(require (quote quack))

;; Ignorer la case lors de recherches
(setq case-fold-search t)
;; Environnement de travail
(set-language-environment "UTF-8")

;; Quand on est sur une parenthèse ouvrante, surligne la parenthèse fermante dans un mode de programmation
(add-hook 'prog-mode-hook 'show-paren-mode)
(setq show-paren-delay 0)
(setq show-paren-style `parenthesis)

;; Ouvre un fichier compressé et écrit comme s'il était décompressé
(auto-compression-mode t)
;; Modification des couleurs dans un code (syntax highlighting)
(global-font-lock-mode t)

(setq quack-default-program "mit-scheme")
;; Crochet ouvrant donne parenthèse
(setq quack-smart-open-paren-p t)
;; Quand on ouvre un sous-scheme, ouvre en splitant la fenêtre
;; (et pas dans un autre fenêtre emacs).
(setq quack-switch-to-scheme-method (quote other-window))
